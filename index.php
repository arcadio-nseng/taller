<?php

require_once "php/dbconect.php";

$testimonials = $connection->query("SELECT * FROM testimonios")->fetchAll(PDO::FETCH_ASSOC);

?>


<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, user-scalable=no" name="viewport">
    <meta content="ie=edge" http-equiv="X-UA-Compatible">
    <title>INSTTIC | Instituto superior de telecomunicaciones, tecnología, información y comunicación</title>
    <meta content="INSTTIC  es un centro de formación superior en telecomunicaciones y nuevas tecnologías
    emplazadoo en la ciudad de Djibloho en el interior de Guinea Ecuatorial" name="description">

    <!-- Etiquetas para compartir en redes sociales -->
    <meta content="es_ES" property="og:locale"/>
    <meta content="website" property="og:type"/>
    <meta content="INSTTIC | Instituto Superior de Telecomunicaciones, Tecnología, Información y Comunicación"
          property="og:title"/>
    <meta content="El INSTTIC es un centro de formación superior en telecomunicaciones y
    nuevas tecnologías emplazado en la ciudad de Djibloho en el interior de Guinea Ecuatorial"
          property="og:description"/>
    <meta content="https://www.insttic.gq" property="og:url"/>
    <meta content="INSTTIC" property="og:site_name"/>
    <meta content="https://www.insttic.gq/img/logo.png" property="og:image"/>

    <!--    App Icons-->
    <link href="img/icons/apple-icon-57x57.png" rel="apple-touch-icon" sizes="57x57">
    <link href="img/icons/apple-icon-60x60.png" rel="apple-touch-icon" sizes="60x60">
    <link href="img/icons/apple-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
    <link href="img/icons/apple-icon-76x76.png" rel="apple-touch-icon" sizes="76x76">
    <link href="img/icons/apple-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
    <link href="img/icons/apple-icon-120x120.png" rel="apple-touch-icon" sizes="120x120">
    <link href="img/icons/apple-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">
    <link href="img/icons/apple-icon-152x152.png" rel="apple-touch-icon" sizes="152x152">
    <link href="img/icons/apple-icon-180x180.png" rel="apple-touch-icon" sizes="180x180">
    <link href="img/icons/android-icon-192x192.png" rel="icon" sizes="192x192" type="image/png">
    <link href="img/icons/favicon-32x32.png" rel="icon" sizes="32x32" type="image/png">
    <link href="img/icons/favicon-96x96.png" rel="icon" sizes="96x96" type="image/png">
    <link href="img/icons/favicon-16x16.png" rel="icon" sizes="16x16" type="image/png">
    <link href="img/icons/manifest.json" rel="manifest">
    <meta content="#1b5e20" name="msapplication-TileColor">
    <meta content="img/icons/ms-icon-144x144.png" name="msapplication-TileImage">
    <meta content="#1b5e20" name="theme-color">

    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link href="css/style.css" rel="stylesheet">

    <style>
        header {
            height: 100vh;
        }
    </style>

</head>
<body>

<header>
    <div class="top-nav">
        <div class="container">
            <address><i class="fa fa-map-marker"></i> INSTTIC, Djibloho, Guinea Ecuatorial</address>
            <nav class="social-links">
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
                <a href=""><i class="fa fa-instagram"></i></a>
                <a href=""><i class="fa fa-sign-in"></i> login</a>
            </nav>
        </div>
    </div>
    <div class="menu-slider-wrapper">
        <div class="navigation">
            <div class="container">
                <figure class="logo">
                    <img alt="logo insttic" src="img/logo.png">
                    <figcaption>INSTTIC</figcaption>
                </figure>
                <nav class="menu">
                    <button class="btn btn-outline-info" id="btn-menu"><i class="fa fa-bars"></i></button>
                    <ul id="menu">
                        <li><a class="active" href="./">Inicio</a></li>
                        <li><a href="acerca-de.html">Acerca de</a></li>
                        <li><a href="formacion.html">Formación</a></li>
                        <li><a href="campus.html">Campus</a></li>
                        <li><a href="admision.html">Admisiones</a></li>
                        <li><a href="galeria.html">Galería</a></li>
                        <li><a href="contacto.php">Contacto</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="slider">
            <div class="carousel slide" data-ride="carousel" id="carouselExampleCaptions">
                <ol class="carousel-indicators">
                    <li class="active" data-slide-to="0" data-target="#carouselExampleCaptions"></li>
                    <li data-slide-to="1" data-target="#carouselExampleCaptions"></li>
                    <li data-slide-to="2" data-target="#carouselExampleCaptions"></li>
                </ol>
                <div class="carousel-inner ">
                    <div class="carousel-item active">
                        <!--<img src="img/slide1.jpg" class="d-block w-100" alt="...">-->
                        <div class="carousel-caption ">
                            <h5>First slide label</h5>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <<!--<img src="img/slide1.jpg" class="d-block w-100" alt="...">-->
                        <div class="carousel-caption ">
                            <h5>Second slide label</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <!--<img src="img/slide1.jpg" class="d-block w-100" alt="...">-->
                        <div class="carousel-caption ">
                            <h5>Third slide label</h5>
                            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" data-slide="prev" href="#carouselExampleCaptions" role="button">
                    <span aria-hidden="true" class="carousel-control-prev-icon"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" data-slide="next" href="#carouselExampleCaptions" role="button">
                    <span aria-hidden="true" class="carousel-control-next-icon"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</header>

<main>
    <section class="welcome">
        <div class="container">
            <h2 class="section-title"><span>B</span>ienvenidos al <span>INSTTIC</span></h2>
            <div class="row">
                <div class="col-12 col-md-6 mb-4">
                    <div class="about mb-4">
                        <h3><strong>Acerca de <span><i>INSTTIC</i></span></strong></h3>
                        <p class="text-black-50 lead border-bottom pb-4">Lorem ipsum dolor sit amet, consectetur
                            adipisicing elit. Aspernatur cupiditate dignissimos
                            eum eveniet fugit illo molestias nam, omnis praesentium tempora. Eaque fuga illum ullam
                            velit!</p>
                        <p class="text-black-50 lead">Laborum numquam placeat quod temporibus? Accusantium atque aut
                            cumque doloremque doloribus
                            esse ex explicabo iste magni molestias nisi numquam quaerat quibusdam rem, rerum sint
                            vero.</p>
                        <a class="btn btn-sm btn-info" href="">Saber más <i class="fa fa-caret-right"></i></a>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="stats p-5">
                        <table class="w-100 text-center text-white">
                            <tr class="mb-4 border-bottom">
                                <td class="p-4 border-right"><strong class="h2">250</strong><br><span
                                            class="text-white-50">item description</span></td>
                                <td class="p-4"><strong class="h2">251</strong><br><span class="text-white-50">item description</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="p-4 border-right"><strong class="h2">250</strong><br><span
                                            class="text-white-50">item description</span></td>
                                <td class="p-4"><strong class="h2">251</strong><br><span class="text-white-50">item description</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="certs pb-4">
        <div class="container">
            <h2 class="section-title"><span class="text-white">C</span>arreras y <span class="text-white">C</span>ertificaciones
            </h2>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card mb-4">
                        <div class="card-header text-center"><i class="fa fa-globe"></i></div>
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional
                                content.</p>
                            <a class="btn btn-primary" href="#">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card mb-4">
                        <div class="card-header text-center"><i class="fa fa-globe"></i></div>
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional
                                content.</p>
                            <a class="btn btn-primary" href="#">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card mb-4">
                        <div class="card-header text-center"><i class="fa fa-globe"></i></div>
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional
                                content.</p>
                            <a class="btn btn-primary" href="#">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card mb-4">
                        <div class="card-header text-center"><i class="fa fa-globe"></i></div>
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional
                                content.</p>
                            <a class="btn btn-primary" href="#">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card mb-4">
                        <div class="card-header text-center"><i class="fa fa-globe"></i></div>
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional
                                content.</p>
                            <a class="btn btn-primary" href="#">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card mb-4">
                        <div class="card-header text-center"><i class="fa fa-globe"></i></div>
                        <div class="card-body">
                            <h5 class="card-title">Special title treatment</h5>
                            <p class="card-text">With supporting text below as a natural lead-in to additional
                                content.</p>
                            <a class="btn btn-primary" href="#">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <h2 class="section-title">Eventos Importantes</h2>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="event d-flex flex-wrap">
                        <figure class="text-center">
                            <figcaption class="d-inline-block pt-2 pb-2 pl-4 pr-4 text-white h4"
                                        style="background-color: rgba(252,99,28,.7)">
                                <time datetime="2019-12-20T15:30">20 Dic</time>
                            </figcaption>
                        </figure>
                        <div class="">
                            <h3 class="text-warning">Lorem ipsum dolor sit amet, consectetur.</h3>
                            <p class="mb-0 pb-0"><i class="fa fa-clock-o"></i> 19:00 - 21:00</p>
                            <p><i class="fa fa-map-marker"></i> INSTTIC, Djibloho</p>
                            <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Doloremque, eaque!</p>
                        </div>
                    </div>
                    <div class="event d-flex flex-wrap">
                        <figure class="text-center">
                            <figcaption class="d-inline-block pt-2 pb-2 pl-4 pr-4 text-white h4"
                                        style="background-color: rgba(45,160,255,.7)">
                                <time datetime="2019-12-20T15:30">20 Dic</time>
                            </figcaption>
                        </figure>
                        <div class="">
                            <h3 class="text-info">Lorem ipsum dolor sit amet, consectetur.</h3>
                            <p class="mb-0 pb-0"><i class="fa fa-clock-o"></i> 19:00 - 21:00</p>
                            <p><i class="fa fa-map-marker"></i> INSTTIC, Djibloho</p>
                            <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Doloremque, eaque!</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="event d-flex flex-wrap">
                        <figure class="text-center">
                            <figcaption class="d-inline-block pt-2 pb-2 pl-4 pr-4 text-white h4"
                                        style="background-color: rgba(252,99,28,.7)">
                                <time datetime="2019-12-20T15:30">20 Dic</time>
                            </figcaption>
                        </figure>
                        <div class="">
                            <h3 class="text-warning">Lorem ipsum dolor sit amet, consectetur.</h3>
                            <p class="mb-0 pb-0"><i class="fa fa-clock-o"></i> 19:00 - 21:00</p>
                            <p><i class="fa fa-map-marker"></i> INSTTIC, Djibloho</p>
                            <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Doloremque, eaque!</p>
                        </div>
                    </div>
                    <div class="event d-flex flex-wrap">
                        <figure class="text-center">
                            <figcaption class="d-inline-block pt-2 pb-2 pl-4 pr-4 text-white h4"
                                        style="background-color: rgba(252,99,28,.7)">
                                <time datetime="2019-12-20T15:30">20 Dic</time>
                            </figcaption>
                        </figure>
                        <div class="">
                            <h3 class="text-warning">Lorem ipsum dolor sit amet, consectetur.</h3>
                            <p class="mb-0 pb-0"><i class="fa fa-clock-o"></i> 19:00 - 21:00</p>
                            <p><i class="fa fa-map-marker"></i> INSTTIC, Djibloho</p>
                            <p class="text-black-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Doloremque, eaque!</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="advantages mt-5">
        <div class="bg pb-5">
            <div class="container">
                <h2 class="section-title text-white">Ventajas de <i class="text-success">estudiar en INSTTIC</i></h2>

                <div class="row">
                    <div class="col-12 col-md-8">
                        <div class="media">
                            <i class="fa fa-check h1 text-success"></i>
                            <div class="media-body ml-2">
                                <h5 class="mt-0 text-white">Lorem ipsum dolor sit amet, consectetur adipisicing
                                    elit.</h5>
                                <p class="text-white-50">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                        <div class="media">
                            <i class="fa fa-check h1 text-success"></i>
                            <div class="media-body ml-2">
                                <h5 class="mt-0 text-white">Lorem ipsum dolor sit amet, consectetur adipisicing
                                    elit.</h5>
                                <p class="text-white-50">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                        <div class="media">
                            <i class="fa fa-check h1 text-success"></i>
                            <div class="media-body ml-2">
                                <h5 class="mt-0 text-white">Lorem ipsum dolor sit amet, consectetur adipisicing
                                    elit.</h5>
                                <p class="text-white-50">Lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                        <a class="btn btn-sm btn-info" href="">Más información <i class="fa fa-caret-right"></i></a>
                    </div>
                    <div class="col-md-4 ">
                        <img src="img/graduate.png" class="d-none d-md-inline img-fluid" alt="">
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="mb-4">
        <div class="container">
            <h2 class="section-title">Testimonios</h2>
            <!-- Swiper -->
            <div class="swiper-container">
                <div class="swiper-wrapper">

                    <?php

                    foreach ($testimonials as $testimonial):
                        $name = $testimonial['NAME'];
                        $position = $testimonial['POSITION'];
                        $test = $testimonial['TESTIMONIAL'];
                        $img = $testimonial['IMG'];

                    ?>

                    <div class="swiper-slide">
                        <div class="media">
                            <img src="img/<?php echo $img?>" class="mr-3" alt="">
                            <div class="media-body">
                                <h5 class="mt-0"><?php echo $name?></h5>
                                <p class="lead"><?php echo $position?></p>
                                <p><?php echo $test?></p>
                            </div>
                        </div>
                    </div>

                    <?php
                    endforeach;
                    ?>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </section>
</main>

<footer>
    <div class="container">
        <div class="footer-items text-center pt-4 pb-4 d-flex flex-wrap justify-content-around">
            <div class="footer-item pb-2">
                <span class="title h4 text-white"><strong>Dirección</strong></span>
                <address class="text-white-50">Ciudad de La Paz, Djibloho <br> Guinea Ecuatorial</address>
            </div>
            <div class="footer-item pb-2">
                <span class="title h4 text-white"><strong>Contacto</strong></span>
                <address><i class="fa fa-phone"></i>: <a href="tel:+240123456789">+240 123 456 789</a></address>
                <address><i class="fa fa-envelope"></i>: <a href="mailto:info@insttic.gq">info@insttic.gq</a></address>
            </div>
            <div class="footer-item pb-2">
                <span class="title h4 text-white"><strong>Horario de apertura</strong></span>
                <p class="text-white-50">Lunes a viernes: 08:00 - 17:00 <br> Cerrado los fines de semana</p>
            </div>
        </div>
        <div class="partners mb-4">
            <a href=""><img alt="Logo Ministerio" src="img/logo_ministerio.png"
                            title="Ministerio de Transportes, Tecnología, Correos y Telecomunicaciones"></a>
            <a href=""><img alt="Logo CISCO" src="img/logo_cisco.png" title="CISCO"></a>
            <a href=""><img alt="Logo ZTE" src="img/logo_zte.png" title="ZTE"></a>
            <a href=""><img alt="Logo Xidian" src="img/logo_xidian.png" title="Xidian University"></a>
        </div>
        <span class="title thanks d-block text-center text-white h4 mb-4">
            <strong><em>¡GRACIAS POR SU VISITA!</em></strong>
        </span>
    </div>
    <div class="footer text-white pt-2 pb-2">
        <div class="container d-flex flex-wrap justify-content-between align-items-center">
            <address>&copy; 2020 INSTTIC - Todos los derechos reservados | developed by <a
                        href="mailto:info@arcadio.gq">@arcadio</a></address>
            <div class="social-btns">
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-instagram"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
</footer>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/swiper.min.js"></script>

<script>
    let $btnMenu = $('#btn-menu');
    $btnMenu.on('click', () => {
        $('#menu').slideToggle();
    });
    $btnMenu.on('blur', () => {
        $('#menu').slideUp();
    })
</script>

<!-- Initialize Swiper -->
<script>
    let swiper = new Swiper('.swiper-container', {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
</script>
</body>
</html>
