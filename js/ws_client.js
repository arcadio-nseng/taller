let chatForm = document.getElementById('chat-form');
let chatInput = document.getElementById('chatInput');
let btnSend = document.getElementById('btn-send');
let chatBox = document.getElementById('chat-box');
let userCount = document.getElementById('user-count');

/**
 * poner al formulario a la escucha del evento submit
 * @param ev
 */
chatForm.onsubmit = function (ev) {

    ev.preventDefault();

    let msg = chatInput.value;
    let msgBoxSend = '<div class="alert alert-secondary w-75 pull-right">'+ msg +'</div>';
    chatBox.innerHTML += msgBoxSend;
    chatInput.value = "";

    let msgFrame = JSON.stringify({type: "message", content: msg});

    socket.send(msgFrame);

};




// Crea una conexión WebSocket.
const socket = new WebSocket('ws://localhost:8080');

// se ejecuta al realizar la conexión
socket.onopen = function () {
    console.log("te has conectado al servidor");

    chatBox.innerHTML = "";
    chatInput.disabled = false;
    btnSend.disabled = false;
};

socket.onmessage = function (ev) {
    let frame = JSON.parse(ev.data);

    if (frame.type === 'control'){
        userCount.innerHTML = frame.content + " usuarios conectados";
    } else if (frame.type === 'message'){

        let msg = frame.content;
        let msgBoxReceive = '<div class="alert alert-primary w-75 pull-left">'+ msg +'</div>';

        chatBox.innerHTML += msgBoxReceive;

    } else if (frame.type === 'update'){

        userCount.innerHTML = frame.users + " usuarios conectados";

        let msgs = frame.content;

        for (let i = 0; i < msgs.length; i++){

            let msgFrame = JSON.parse(msgs[i]);

            let msg = msgFrame.content;
            let msgBoxReceive = '<div class="alert alert-primary w-75 pull-left">'+ msg +'</div>';
            chatBox.innerHTML += msgBoxReceive;


        }

    }
};

socket.onerror = function () {
    console.error("Se ha producido un error inesperado");
};

socket.onclose = function () {
    console.info("has perdido la conexión con el servidor");
};

