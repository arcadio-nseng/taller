<?php

require_once 'config.php';

try{

    $connection = new PDO('mysql:host='.HOST.'; dbname='.DDBB, USER, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $connection->exec('SET CHARACTER SET UTF8');

} catch (Exception $error){

    die("Error al conectar con la base de datos: ". $error->getMessage());

}
