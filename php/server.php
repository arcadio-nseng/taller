<?php

use vakata\websocket\Server;

require_once "ws/autoload.php";

/**
 * instancia del servidor websocket
 */
$server = new Server('ws://127.0.0.1:8080');

/**
 * variable array que contine los mensajes enviados
 */
$messages = array();

/**
 * este evento se produce cuando un cliente envíe un mensaje al servidor. en este caso el manejador reenviará el mensaje
 * a todos los clientes conectados excepto al que remitente
 */
$server->onMessage(function ($sender, $message, $server) {

    global $messages;

    $messages [] =  $message;

    foreach ($server->getClients() as $client) {
        if ((int)$sender['socket'] !== (int)$client['socket']) {
            $server->send($client['socket'], $message);
        }
    }
});

/**
 * este evento se produce cuando cuando un cliente se conecta al servidor
 */
$server->onConnect(function ($user){
    global $server;
    global $messages;

    $conectedUsers = count($server->getClients()) - 1;

    $frameControl = json_encode(array("type" => "control", "content"=>$conectedUsers));
    $updateFrame = json_encode(array("type" => "update", "content"=>$messages, "users"=>$conectedUsers));

    foreach ($server->getClients() as $client) {

        if ((int)$client['socket'] !== (int)$user['socket']) {
            $server->send($client['socket'], $frameControl);
        }
    }

    $server->send($user['socket'], $updateFrame);
});

/**
 * este método pone al servidor en ejecución en un bucle infinito
 */
$server->run();
