<?php
if (isset($_COOKIE["sesion_insttic"])){

    session_name("sesion_insttic");
    session_start();
}else{
    header("location: contacto.php");
}

?>


<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contacto | INSTTIC</title>
    <meta name="description" content="INSTTIC  es un centro de formación superior en telecomunicaciones y nuevas tecnologías
    emplazadoo en la ciudad de Djibloho en el interior de Guinea Ecuatorial">

    <!-- Etiquetas para compartir en redes sociales -->
    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="INSTTIC | Instituto Superior de Telecomunicaciones, Tecnología, Información y Comunicación" />
    <meta property="og:description" content="El INSTTIC es un centro de formación superior en telecomunicaciones y
    nuevas tecnologías emplazado en la ciudad de Djibloho en el interior de Guinea Ecuatorial"/>
    <meta property="og:url" content="https://www.insttic.gq" />
    <meta property="og:site_name" content="INSTTIC" />
    <meta property="og:image" content="https://www.insttic.gq/img/logo.png" />

    <!--    App Icons-->
    <link rel="apple-touch-icon" sizes="57x57" href="img/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icons/favicon-16x16.png">
    <link rel="manifest" href="img/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#1b5e20">
    <meta name="msapplication-TileImage" content="img/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#1b5e20">

    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">

    <style>
        .btn-chat{
            position: fixed;
            bottom: 2rem;
            right: 2rem;
            z-index: 10;
        }
    </style>


</head>
<body>

<button class="btn btn-primary btn-chat" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-comment"></i></button>

<header>
    <div class="top-nav">
        <div class="container">
            <address><i class="fa fa-map-marker"></i> INSTTIC, Djibloho, Guinea Ecuatorial</address>
            <nav class="social-links">
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
                <a href=""><i class="fa fa-instagram"></i></a>
                <a href=""><i class="fa fa-sign-in"></i> login</a>
            </nav>
        </div>
    </div>
    <div class="menu-slider-wrapper">
        <div class="navigation">
            <div class="container">
                <figure class="logo">
                    <img src="img/logo.png" alt="logo insttic">
                    <figcaption>INSTTIC</figcaption>
                </figure>
                <nav class="menu">
                    <button class="btn btn-outline-info" id="btn-menu"><i class="fa fa-bars"></i></button>
                    <ul id="menu">
                        <li><a href="./">Inicio</a></li>
                        <li><a href="acerca-de.html" class="active">Acerca de</a></li>
                        <li><a href="formacion.html">Formación</a></li>
                        <li><a href="campus.html">Campus</a></li>
                        <li><a href="admision.html">Admisiones</a></li>
                        <li><a href="galeria.html">Galería</a></li>
                        <li><a href="contacto.php">Contacto</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="banner">
            <div class="container pt-4 pb-4">
                <div class="media">
                    <i class="fa fa-phone text-white"></i>
                    <div class="media-body ml-3">
                        <h2 class="mt-0 title text-white">Contacta con nosotros</h2>
                        <p class="lead text-white-50">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consequatur cupiditate
                            expedita, in iste, laboriosam molestias mollitia nemo nihil nisi perspiciatis rerum soluta
                            velit, voluptate?</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<main>
    <section class="contact mb-4">
        <div class="container">
            <h2 class="section-title">Envíanos un mensaje</h2>
            <form action="contacto.php" method="" id="contact-form">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-user"></i></div>
                    </div>
                    <input type="text" class="form-control" name="name"  id="name" placeholder="Nombre y apellido" required>
                </div>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">@</div>
                    </div>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Correo electrónico" required>
                </div>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fa fa-pencil-square"></i></div>
                    </div>
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Username" required>
                </div>
                <div class="input-group mb-2">
                    <textarea name="msg" id="msg" cols="30" rows="10" class="form-control" placeholder="Redacta tu mensajer..." required></textarea>
                </div>

                <button class="btn btn-primary" type="submit" id="btn-submit">Enviar <i class="fa fa-send"></i></button>
                <button class="btn btn-primary d-none" type="button" id="btn-sending" disabled>
                    <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Enviando...
                </button>

            </form>
        </div>
    </section>
</main>

<footer>
    <div class="container">
        <div class="footer-items text-center pt-4 pb-4 d-flex flex-wrap justify-content-around">
            <div class="footer-item pb-2">
                <span class="title h4 text-white"><strong>Dirección</strong></span>
                <address class="text-white-50">Ciudad de La Paz, Djibloho <br> Guinea Ecuatorial</address>
            </div>
            <div class="footer-item pb-2">
                <span class="title h4 text-white"><strong>Contacto</strong></span>
                <address><i class="fa fa-phone"></i>: <a href="tel:+240123456789">+240 123 456 789</a></address>
                <address><i class="fa fa-envelope"></i>: <a href="mailto:info@insttic.gq">info@insttic.gq</a></address>
            </div>
            <div class="footer-item pb-2">
                <span class="title h4 text-white"><strong>Horario de apertura</strong></span>
                <p class="text-white-50">Lunes a viernes: 08:00 - 17:00 <br> Cerrado los fines de semana</p>
            </div>
        </div>
        <div class="partners mb-4">
            <a href=""><img src="img/logo_ministerio.png" alt="Logo Ministerio"
                            title="Ministerio de Transportes, Tecnología, Correos y Telecomunicaciones"></a>
            <a href=""><img src="img/logo_cisco.png" alt="Logo CISCO" title="CISCO"></a>
            <a href=""><img src="img/logo_zte.png" alt="Logo ZTE" title="ZTE"></a>
            <a href=""><img src="img/logo_xidian.png" alt="Logo Xidian" title="Xidian University"></a>
        </div>
        <span class="title thanks d-block text-center text-white h4 mb-4">
            <strong><em>¡GRACIAS POR SU VISITA!</em></strong>
        </span>
    </div>
    <div class="footer text-white pt-2 pb-2">
        <div class="container d-flex flex-wrap justify-content-between align-items-center">
            <address>&copy; 2020 INSTTIC - Todos los derechos reservados | developed by <a
                    href="mailto:info@arcadio.gq">@arcadio</a></address>
            <div class="social-btns">
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-instagram"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
            </div>
        </div>
    </div>
</footer>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Bienvenido al chat <?php echo $_SESSION["usuario"]?> <span class="text-success" id="user-count">(5 usuarios en linea)</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="chat-box">
                <div class="alert alert-primary w-75 pull-left">hola qué tal</div>
                <div class="alert alert-secondary w-75 pull-right">bien y tú</div>
            </div>
            <div class="modal-footer">
                <form id="chat-form" class="d-flex w-100">
                    <input type="text" class="form-control w-100" id="chatInput"
                           placeholder="Escribe tu mensaje .." disabled required>
                    <button class="btn btn-success" type="submit" id="btn-send" disabled><i class="fa fa-send"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script>
    let $btnMenu = $('#btn-menu');
    let username = "<?php echo $_SESSION["usuario"]?>";
    $btnMenu.on('click', ()=>{
        $('#menu').slideToggle();
    });
    $btnMenu.on('blur', ()=>{
        $('#menu').slideUp();
    })
</script>

<script>
    $("#contact-form").on('submit', function (e) {
        e.preventDefault();

        let name = document.getElementById('name').value;
        let email = document.getElementById('email').value;
        let subject = document.getElementById('subject').value;
        let msg = document.getElementById('msg').value;
        let btnSubmit = document.getElementById('btn-submit');
        let btnSending = document.getElementById('btn-sending');

        let data = {
            name: name,
            email: email,
            subject: subject,
            msg: msg
        };

        btnSubmit.classList.add('d-none');
        btnSending.classList.remove('d-none');

        $.post('php/send_msg.php', data, function (result) {
            btnSending.classList.add('d-none');
            btnSubmit.classList.remove('d-none');
            if(result.success) alert('mensaje enviado con éxito');
            else alert('Error al eviar el mensaje');
        }, 'json').fail(function (xhr) {
            console.error(xhr.responseText);
        })

    });
</script>
<script src="js/ws_client.js"></script>
</body>
</html>
